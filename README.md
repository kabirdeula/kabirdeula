# Hi there, I'm Kabir Deula

## I'm a Bachelor student at [NCCS](//nccs.edu.np), majoring in BCA

## Connect with me

[<img src="https://img.shields.io/badge/-Website-black?style=for-the-badge&logo=brave">][website]
[<img src="https://img.shields.io/badge/-Facebook-white?style=for-the-badge&logo=facebook">][facebook]
[<img src="https://img.shields.io/badge/-Instagram-pink?style=for-the-badge&logo=instagram">][instagram]
[<img src="https://img.shields.io/badge/-GitHub-green?style=for-the-badge&logo=github">][github]

[website]: https://kabirdeula.com.np
[facebook]: http://facebook.com/kabirdeula167
[instagram]: https://instagram.com/king_dragon2021/
[github]: https://github.com/kabirdeula